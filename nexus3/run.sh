#!/bin/bash

docker run --name nexus3 -d -p 8081:8081 -v /mnt/hd1/sonatype/:/opt/sonatype/sonatype-work --restart unless-stopped nexus3
